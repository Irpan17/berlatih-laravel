<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('data-tables', function(){
    return view('data-tables');
});

Route::get('table', function(){
    return view('table');
});

Route::get('/', 'AwalController@dashboard');
Route::get('/Register', 'AuthController@pendataan');
Route::post('/welcome', 'AuthController@welcome');

// CRUD cast
// mengarah ke form create cast 
Route::get('/cast/create', 'castController@create');
// menyinpan data ke tabel cast
Route::post('/cast', 'castController@store');
// tampil semua data
Route::get('/cast', 'castController@index');
// detail data
Route::get('/cast/{cast_id}', 'castController@show');
// mengarah ke form edit data
Route::get('/cast/{cast_id}/edit', 'castController@edit');
// update data ke tabel cast
Route::put('/cast/{cast_id}', 'castController@update');
// destroy/delete data cast
Route::delete('/cast/{cast_id}', 'castController@destroy');


Route::group(['middleware' => ['auth']], function () {
    // CRUD genre
    // mengarah ke form create genre 
    Route::get('/genre/create', 'genreController@create');
    // menyinpan data ke tabel genre
    Route::post('/genre', 'genreController@store');
    // tampil semua data
    Route::get('/genre', 'genreController@index'); 
    // detail data
    Route::get('/genre/{genre_id}', 'genreController@show');
    // mengarah ke form edit data
    Route::get('/genre/{genre_id}/edit', 'genreController@edit');
    // update data ke tabel genre
    Route::put('/genre/{genre_id}', 'genreController@update');
    // destroy/delete data genre
    Route::delete('/genre/{genre_id}', 'genreController@destroy');
    // Profile
    Route::resource('profile', 'profileController')->only([
        'index', 'update'
    ]);
    Route::resource('kritik', 'kritikController')->only([
        'store'
    ]);
});


Route::resource('film', 'filmController');

Auth::routes();
