@extends('layout.master')
@section('judul')
Buat Account Baru
@endsection
<h3>Sign Up Form</h3>
@section('content')
<form action="/welcome" method="post">
    @csrf
    <label>First name:</label><br>
    <input type="text" name="name1"><br><br>
    <label>Last name:</label><br>
    <input type="text" name="name2"><br><br>
    <label>Gender</label><br>
    <input type="radio">Male<br>
    <input type="radio">Female<br><br>
    <label>Nationality</label>
    <select name="Nationality">
    <option value="Indonesia">Indonesia</option>
    <option value="Amerika">Amerika</option>
    <option value="Inggris">Inggris</option>
    </select> <br> <br>
    <label>Language Spoken</label><br>
    <input type="checkbox" name="Language">Indonesia<br>
    <input type="checkbox" name="Language">English<br>
    <input type="checkbox" name="Language">Other<br><br>
    <label>Bio</label><br>
    <textarea name="Bio" rows="10" cols="30"></textarea><br><br>
    <input type="submit" value="sign up"></form>
@endsection