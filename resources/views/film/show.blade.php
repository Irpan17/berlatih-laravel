@extends('layout.master')
@section('judul')
Halaman Detail Film
@endsection

@section('content')
<div class="row">
    <div class="col-4">
        <div class="card">        
            <img src="{{asset('poster/' . $film->poster)}}" class="card-img-top" >            
            <h5 class="card-title btn btn-dark container">{{$film->judul}}</h5>
            <p class="card-text">{{ $film->ringkasan }}</p>
            <a href="/film" class="btn btn-success btn-sm">Kembali</a>
            
            <div class="card-footer">
                <small class="text-muted">Last Updated {{$film->tahun}}</small>
            </div>
        </div>          
    </div>    
</div>
<h1>List Kritik</h1>

@forelse ($film->kritik as $item)
    <div class="card">
        <div class="card-header">
        {{$item->user->name}}
        </div>
        <div class="card-body">
        <h5 class="card-title">{{$item->content}} </h5>
        <p class="card-text">{{$item->point}} Point</p>
        </div>
    </div>
@empty
   <h1>Belum Ada Kritik</h1> 
@endforelse
    <form action="/kritik" method="post">
        @csrf
        <input type="hidden" value="{{$film->id}}" name="film_id">        
            <div class="form-group">
                <label>Content Kritik</label>
                <textarea name="content" class="form-control"></textarea>
            </div><div class="form-group">
            @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

                <label>Point</label>
                <input type="number" class="form-control" name="point">
            </div>
            @error('Point')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        <input type="submit" value="Tambah">
    </form>    
@endsection