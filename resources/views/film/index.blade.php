@extends('layout.master')
@section('judul')
Halaman List Film
@endsection

@section('content')

@auth
<a href="/film/create" class="btn btn-info my-2">Tambah Film</a>
@endauth

<div class="row">
    @forelse ($film as $item)
    <div class="col-4">
        <div class="card">        
            <img src="{{asset('poster/' . $item->poster)}}" class="card-img-top" width="50px" height="300px">
            <div class="card-body">
                <h1 class="card-title badge badge-dark container">{{$item->judul}}</h1>
                <span class="badge badge-secondary">{{$item->genre->nama}}</span>
                <p class="card-text">{{ Str::limit($item->ringkasan, 35) }}</p>
                @auth
                <form action="/film/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/film/{{$item->id}}" class="btn btn-success btn-sm">Detail</a>
                    <a href="/film/{{$item->id}}/edit" class="btn btn-dark btn-sm">Edit</a>
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form> 
                @endauth
            </div>
            <div class="card-footer">
                <small class="text-muted">Last Updated {{$item->tahun}}</small>
            </div>
            @guest
            <a href="/film/{{$item->id}}" class="btn btn-success btn-sm">Detail</a>                
            @endguest
        </div>          
    </div>
      @empty
          <h1>DATA KOSONG</h1>
      @endforelse
</div>  
  
@endsection