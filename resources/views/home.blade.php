@extends('layout.master')
@section('judul')
SANBERBOOK
@endsection
@section('content')
<h2>SOSIAL MEDIA DEVELOPER SANTAI BERKUALITAS</h2>

<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

<h2>Benefit Join di Sanberbook</h2>

<ul>
    <li>Mendapatkan motivasi dari sesama Developer</li> 
    <li>Sharing knowledge</li>
    <li>Dibuat oleh calon developer terbaik</li>
</ul>

<h2>Cara Bergabung ke Sanberbook</h2>

<ol>
    <li>Mengunjungi Website</li>
    <li>Mendaftarkan di <a href="/Register"> Form sign up </a>
    <li>Selesai</li>
@endsection
