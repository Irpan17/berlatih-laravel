@extends('layout.master')
@section('judul')
Halaman Detail Genre
@endsection

@section('content')

<h1>{{$genre->nama}}</h1>
<p>{{$genre->ringkasan}}</p>

<div class="row">
    @forelse ($genre->film as $item)
    <div class="col-4">
        <div class="card">        
            <img src="{{asset('poster/' . $item->poster)}}" class="card-img-top" width="50px" height="300px">
            <div class="card-body">
                <h1> {{$item->judul}}</h1>
                <p class="card-text">{{ Str::limit($item->ringkasan, 35) }}</p>
            </div>
            <div class="card-footer">
                <small class="text-muted">Last Updated {{$item->tahun}}</small>
            </div>
            <a href="/film/{{$item->id}}" class="btn btn-success btn-sm">Detail</a>
        </div>          
    </div>
    @empty
        <h1>Tidak ada Film</h1>
    @endforelse    
</div>

{{-- @forelse ($genre->film as $item)
<div class="col-4">
    <div class="card">        
        <img src="{{asset('poster/' . $item->poster)}}" class="card-img-top" width="50px" height="300px">
        <div class="card-body">
            <h1 class="card-title badge badge-dark container">{{$item->judul}}</h1>
            <p class="card-text">{{ Str::limit($item->ringkasan, 35) }}</p>
            @auth
            <form action="/film/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <a href="/film/{{$item->id}}" class="btn btn-success btn-sm">Detail</a>
                <a href="/film/{{$item->id}}/edit" class="btn btn-dark btn-sm">Edit</a>
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form> 
            @endauth
        </div>
        <div class="card-footer">
            <small class="text-muted">Last Updated {{$item->tahun}}</small>
        </div>
        @guest
        <a href="/film/{{$item->id}}" class="btn btn-success btn-sm">Detail</a>                
        @endguest
    </div>          
</div>
@empty
    <h1>Tidak ada Film</h1>
@endforelse --}}

@endsection