@extends('layout.master')
@section('judul')
Halaman List Genre
@endsection

@section('content')
<a href="/genre/create" class="btn btn-primary my-3">Tambah Genre</a>

<table class="table table-striped table-dark">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>        
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($genre as $key => $item)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->nama}}</td> 
            <td>                  
                <form action="/genre/{{$item->id}}" method="POST">
                  @csrf
                  @method('delete')
                  <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" class="btn btn-danger btn sm" value="Delete">
                </form>
            </td>
          </tr>
      @empty
          <h1>DATA KOSONG</h1>
      @endforelse
    </tbody>
  </table> 
  
@endsection