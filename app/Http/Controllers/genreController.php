<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\genre;

class genreController extends Controller
{
    public function create()
    {
        return view('genre.create');
    }

    public function store(Request $request){
        $request->validate(
            [
                'nama' => 'required',                
            ],
            [
                'nama.required' => 'nama belum diisi',
            ]
        );
        DB::table('genre')->insert(
            ['nama' => $request ['nama'],]
        );
        return redirect('/genre');
    }


    public function index()
    {
        $genre = DB::table('genre')->get();
 
        return view('genre.index', compact('genre'));
    }


    public function show($id)
    {
        $genre = genre::findOrFail($id);

        return view('genre.show', compact('genre'));
    }


    public function edit($id)
    {
        $genre = DB::table('genre')->where('id', $id)->first();

        return view('genre.edit', compact('genre'));
    }


    public function update($id, Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
            ],
            [
                'nama.required' => 'nama belum diisi',
            ]
        );
        DB::table('genre')->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                ]
            );
            return redirect('/genre');
    }


    public function destroy($id)
    {
        DB::table('genre')->where('id', '=', $id)->delete();
        return redirect ('/genre');
    }





}