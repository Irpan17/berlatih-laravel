<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function pendataan()
    {
        return view ('halaman.register');
    }
    public function welcome(Request $request)
    {
        // dd($request->all());
        $name1 = $request['name1'];
        $name2 = $request['name2'];
        return view ('welcome', compact('name1','name2',));
    }
}
