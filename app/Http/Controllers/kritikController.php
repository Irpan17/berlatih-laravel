<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\kritik;

class kritikController extends Controller
{
    public function store(Request $request){
        $request->validate(
            [
                'content' => 'required',
                'point' => 'required',              
            ],
            [
                'content.required' => 'content belum diisi',
                'point.required' => 'point belum diisi',
            ]
        );
        $kritik = new kritik;
        $kritik->content = $request->content;
        $kritik->point = $request->point;            
        $kritik->film_id = $request->film_id;            
        $kritik->user_id = Auth::id();            
        $kritik->save();        
        

        return redirect ('/film/'. $request->film_id);
    }
}
