<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\profile;

class profileController extends Controller
{
    public function index(){
        $id = Auth::id();
        $profile = profile::where('user_id', Auth::id())->first();
        return view('profile.edit', compact('profile'));
    }
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'umur' => 'required',
                'bio' => 'required',
                'alamat' => 'required',              
            ],
            [
                'umur.required' => 'umur belum diisi',
                'bio.required'  => 'bio belum diisi',
                'alamat.required'  => 'alamat belum diisi',
            ]
        );
        $profile = profile::find($id);
        $profile->umur = $request->umur;
        $profile->bio = $request->bio;
        $profile->alamat = $request->alamat;

        $profile->save();
        return redirect('/profile');
    }
}
