<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kritik extends Model
{
    protected $table = "kritik";
    protected $fillable = ['user_id', 'film_id', 'content', 'point'];

    public function film()
    {
        return $this->belongsTo('App\film', 'film_id');
    }

    public function user()
    {
        return $this->belongsTo('App\user', 'user_id');
    }
}
